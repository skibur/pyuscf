import json
import pytest

from pyuscf import __version__
from pyuscf import Msa
from pyuscf import IdTypeError

# Test setup
profile = Msa('12743305')
profile_tmp = json.loads(profile.get_profile().to_json())
profile_data_keys = []
for key in profile_tmp[0].keys():
    profile_data_keys.append(key)


def test_version():
    assert __version__ == '0.2.1'


def test_msa_init_raises():
    with pytest.raises(IdTypeError, match=r".*'str' or 'int'.*"):
        Msa([12743305])


def test_msa_return_attrs():
    test_data = [
        "uscf_id",
        "expires",
        "name",
        "reg_rating",
        "quick_rating",
        "blitz_rating",
        "state_country",
        "fide_id",
        "fide_rating"
    ]
    assert profile_data_keys == test_data


def test_profile_uscf_id():
    assert isinstance(profile.get_uscf_id(), str)


def test_profile_expires_date():
    assert isinstance(profile.get_expires_date(), str)


def test_profile_name():
    assert isinstance(profile.get_name(), str)


def test_profile_regular_rating():
    assert isinstance(profile.get_regular_rating(), str)


def test_profile_quick_rating():
    assert isinstance(profile.get_quick_rating(), str)


def test_profile_blitz_rating():
    assert isinstance(profile.get_blitz_rating(), str)


def test_profile_state_country():
    assert isinstance(profile.get_state_country(), str)


def test_profile_fide_id():
    assert isinstance(profile.get_fide_id(), str)


def test_profile_fide_rating():
    assert isinstance(profile.get_fide_rating(), str)

def test_get_tournament_hosting():
    assert isinstance(profile.get_tournament_hosting(), list)
