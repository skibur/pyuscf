# Change Log

## 0.2.1 - 2019-10-02
### Added
- Added get_tournament_hosting method

## 0.1.1 - 2019-08-19
### Added
- Added int support to Msa() uscf id.
### Changed
- Changed CHANGELOG and README to Markdown markup language.

## 0.1.0 - 2019-08-18
Initial release
